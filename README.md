# Python Developer Test

Here is a web application able to upload, cropping faces and store their gender made with django.
Time expended: 5h

## Getting started 🚀
```
pip install -r requirements.txt
```
```
python manage.py migrate
```
```
python manage.py runserver
```

### Functionalities 📋

The application consists of 2 views. In the first one, you can upload images and see all your storage of images and crops.
In the second view  (^/cropping) you can click an image and cropping it, adding a gender as well.


### Stack used 🔧

I've used a very simple custom-form view instead of creating an API REST.
Maybe it could be better using _django rest framework_ for standardize. I've
gone for the motto "Simple is easier", but choosing a more structured paradigm 
would be better in a big application.


## Comments 📦

I've not found (at a glance) any Python library to __change__ a Image
metadata. Instead of that, I've created a column in the crop table.
I suppose that for training a neuronal network, would be more useful
to have the images with the gender in metadata, to avoiding making
queries to the database.

The application views are unsatisfactory because of the short time. The
masonry library would be a good choice to represent the images in the 
list view.

Author: Marc Bernardo

Date: 25-12-2019