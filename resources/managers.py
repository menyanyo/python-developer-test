import os
import datetime

from PIL import Image

from django.db import models
from django.conf import settings


class CropManager(models.Manager):
    def create_by_cropping(self, photo, x, y, h, w, gender):
        """Create a cropped image into the crops media folder,
        and link that imgage's url to a new Crop object """

        image = Image.open(photo.file)
        cropped_image = image.crop((x, y, w + x, h + y))
        new_filename = "crop{}_{}".format(
            hash(datetime.datetime.now()),
            os.path.basename(photo.file.path)
        )
        new_path = os.path.join(
            settings.MEDIA_ROOT,
            'crops',
            new_filename
        )
        cropped_image.save(new_path)

        return self.create(
            photo=photo,
            cropFile=os.path.join('crops', new_filename),
            gender=gender
        )
