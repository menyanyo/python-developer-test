from django.contrib import admin

from resources.models import Photo, Crop

admin.site.register(Photo)
admin.site.register(Crop)
