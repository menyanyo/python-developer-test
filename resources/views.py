from django.shortcuts import render, redirect
from library.constants import GENDER_CHOICES_LIST
from .models import Photo, Crop
from .forms import PhotoForm, CropForm


def resource_list(request):
    if request.method == 'POST':
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('resource_list')
    else:
        form = PhotoForm()

    data_view = {
        'form': form,
        'photos': Photo.objects.all(),
        'crops': Crop.objects.all(),
        'genders': GENDER_CHOICES_LIST,
    }
    return render(
        request,
        'resource_list.html',
        data_view
    )


def cropping_list(request):
    photos = Photo.objects.all()
    crops = Crop.objects.all()
    if request.method == 'POST':
        form = CropForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('cropping_list')
    else:
        form = CropForm()

    data_view = {
        'form': form,
        'photos': photos,
        'crops': crops,
        'genders': GENDER_CHOICES_LIST,
    }
    return render(
        request,
        'cropping_list.html',
        data_view
    )
