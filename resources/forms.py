from django import forms

from .models import Photo, Crop


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ('file', )
        widgets = {
            'file': forms.FileInput(attrs={
                'accept': 'image/*'
            })
        }


class CropForm(forms.Form):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())
    photoId = forms.CharField(widget=forms.HiddenInput())
    gender = forms.CharField(widget=forms.HiddenInput())

    def save(self, *args, **kwargs):
        """ Creating a cropped image with the data provided by Form"""

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')
        gender = self.cleaned_data.get('gender')
        photo_id = self.cleaned_data.get('photoId')
        photo = Photo.objects.get(id=photo_id)

        # Cropped image creation
        Crop.objects.create_by_cropping(
            photo=photo,
            x=x, y=y, w=w, h=h, gender=gender
        )
