$(function () {
  /* SCRIPT TO ADD A NEW PHOTO */
  $("#id_file").change(function () {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $("#image").attr("src", e.target.result);
        $("#photoFormUpload").submit();
      }
      reader.readAsDataURL(this.files[0]);
    }
  });
});