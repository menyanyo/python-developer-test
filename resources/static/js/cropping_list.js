$(function () {
  /* SCRIPT TO OPEN THE MODAL WITH THE PREVIEW */
  $('.photo').click(function(){
    $("#image").attr("src", this.getAttribute('src'));
    $("#image").attr("photoId", this.getAttribute('data-id'));
    $("#modalCrop").modal("show");
  })

  /* SCRIPTS TO HANDLE THE CROPPER BOX */
  var $image = $("#image");
  var cropBoxData;
  var canvasData;
  $("#modalCrop").on("shown.bs.modal", function () {
    $image.cropper({
      viewMode: 1,
      ready: function () {
        $image.cropper("setCanvasData", canvasData);
        $image.cropper("setCropBoxData", cropBoxData);
      }
    });
  }).on("hidden.bs.modal", function () {
    cropBoxData = $image.cropper("getCropBoxData");
    canvasData = $image.cropper("getCanvasData");
    $image.cropper("destroy");
  });
  $(".js-zoom-in").click(function () {
    $image.cropper("zoom", 0.1);
  });
  $(".js-zoom-out").click(function () {
    $image.cropper("zoom", -0.1);
  });
  /* SCRIPT TO COLLECT THE DATA AND POST TO THE SERVER */
  $(".js-crop-and-upload").click(function () {
    var cropData = $image.cropper("getData");
    var gender = $(".gender-selector option:selected").val();
    $("#id_x").val(cropData["x"]);
    $("#id_y").val(cropData["y"]);
    $("#id_height").val(cropData["height"]);
    $("#id_width").val(cropData["width"]);
    $("#id_gender").val(gender);
    $("#id_photoId").val($image[0].getAttribute("photoId"));
    $("#cropFormUpload").submit();
  });
});