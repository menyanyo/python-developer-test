from django.db import models

from .managers import CropManager


class Photo(models.Model):
    """ Represents each image stored in the database """
    file = models.ImageField(upload_to='photos')


class Crop(models.Model):
    """ Represents a cropped image. """

    photo = models.ForeignKey(
        Photo,
        related_name='crops',
        on_delete=models.CASCADE
    )
    cropFile = models.ImageField(upload_to='crops')
    gender = models.CharField(max_length=200)
    objects = CropManager()
